/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class Task2.
*/
public class Task2 {
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan. method ini tidak memiliki return atau 'void return'
		dan juga modifier static yang memiliki arti bahwa method ini dapat dipanggil tanpa membuat instance
	*/
	public static void main(String[] args) {
    	int age = 25; // Deklarasi & inisialisasi variable 'age' mewakili umur dengan tipe integer dengan nilai 25.
    	int weight = 48;// Deklarasi dan inisialisasi variable 'weight' mewakili berat badan tipe integer dengan nilai 48.
		if (age >= 18) { // melakukan pengecekan kondisi, jika age lebih dari 18, maka jalankan kode dalam if yang tereksekusi
			// karena age memiliki nilai lebih dari 18 maka
			if (weight > 50) { // Melakukan pengecekan apakah 'weight' lebih dari 50, jika 'true' maka jalankan kode berikut :
				System.out.println("You are eligible to donate blood"); // menampilkan atau mencetak tulisan 'You are eligible to donate blood'
			} else { // jika tidak , maka jalankan kode berikut :  
				System.out.println("You are not eligible to donate blood"); // menampilkan atau mencetak tulisan 'You are not eligible to donate blood'
			}
		} else { // jika age kurang dari 18 maka kode else dibawah ini yang akan tereksekusi
			System.out.println("Age must be greater than 18"); // menampilkan atau mencetak tulisan 'Age must be greater than 18'
		}
  	}
}

/*
	Hasil : You are not eligible to donate blood
	Deskripsi: Hal ini karena pada if yang pertama nilai 'age' atau umur memenuhi yaitu lebih dari atau sama dengan 18,
			akan tetapi nested if yang kedua dimana 'weight' atau berat bernilai 'false' karena harus lebih dari 50.
 */
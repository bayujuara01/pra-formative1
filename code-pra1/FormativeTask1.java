/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class Task1.
*/
public class Task1 {
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan .
	*/
	public static void main(String[] args) {
		int i = 1; // Deklarasi dan inisialisasi variable 'i' dengan tipe integer dengan nilai 1.

    	// perulangan do-while dijalankan paling tidak sekali
	do {
		// jika nilai variabel i sama dengan 5, maka yang terjadi ialah: 
      		if (i == 5) {
        		i++; // nilai i akan ditambahkan dengan 1,
        		break; // dan akan dihentikan perulangannya seketika juga.
      		}
      		System.out.println(i); // Mencetak atau menampilkan nilai i sehingga terlihat oleh user
      		i++; // setiap perulangan nilai i akan ditambahkan dengan 1
    	} while (i <= 10); 
	// Perulangan dijalankan selama nilai i kurang dari atau sama dengan 10, atau ketika dijalankan keyword 'break'
  }
}

/*
	Hasil : 1
		2
		3
		4
	Deskripsi : Hasil hanya mencetak 1 sampai 4 dikarenakan, nilai 'i' awal ialah 1, dan
		di tambah 1 (increment) setiap perulangan dan pada saat
		perulangan dimana nilai 'i' sama dengan 5 maka akan terhenti seketika karena
		keyword 'break', serta nilai 'i' di tambah dengan satu, namun tidak ditampilkan
		karena fungsi 'print' berada setelah keyword 'break' dieksekusi.
*/
/*
	Task   : Formative Task 4
	Desc   : Sum two number
	Author : Bayu Seno Ariefyanto
*/

public class FormativeTask4 {
	public static void main(String[] args) {
		long a = Long.parseLong(args[0]);
		long b = Long.parseLong(args[1]);
		System.out.printf("%d + %d = %d", a, b, sum(a,b));
	}

	public static long sum(long a, long b) {
		return a + b;
	}
}
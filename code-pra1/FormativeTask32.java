import java.util.*; // melakukan import semua class pada library java.util yang merupakan java collection framework
/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class Task32.
*/
public class Task32 {
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan dan menerima argument tipe String.
	*/
	public static void main(String args[]) {
		// Deklarasi dan instansiasi variable 'list' dengan, tipe non-primitive ArrayList,
		// yang mana isi dalam ArrayList tersebut harus tipe object 'String'. 
    // ArrayList merupakan object seperti array tapi bersifat dinamis
		ArrayList<String> list = new ArrayList<String>();
		list.add("Mango"); // menambahkan object tipe 'String' dengan value 'Mango' ke list.
		list.add("Apple"); // menambahkan object tipe 'String' dengan value 'Apple' ke list.
		list.add("Banana"); // menambahkan object tipe 'String' dengan value 'Banana' ke list.
		list.add("Grapes"); // menambahkan object tipe 'String' dengan value 'Grapes' ke list.
		
		/*	Menampilkan dan mencetak representasi nilai-nilai atau values yang terdapat pada ArrayList 'list'
		 	Karena secara implisit akan memanggil method .toString() yang dimiliki oleh object ArrayList
		 	dimana mengubah nilai-nilai didalam ArrayList menjadi String.
		 	Sehingga akan menampilkan : [Mango, Apple, Banana, Grapes]
		*/
		System.out.println(list); 
	}
}
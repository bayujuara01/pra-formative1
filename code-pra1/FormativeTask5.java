import java.util.*; // melakukan import semua class pada library java.util yang merupakan java collection framework
/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class HashMapExample1.
*/
public class HashMapExample1{
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan dan menerima argument tipe String.
	*/
	public static void main(String args[]){
		// Deklarasi dan instansiasi variable 'map' dengan, tipe non-primitive HashMap,
		// yang mana isi dalam HashMap terdiri dari Key dengan tipe 'Integer', dan Value
		// dengan tipe 'String', sehingga key dan value harus sesuai tipe ketika menambahkan ke HashMap 'map'
		// HashMap merupakan object yang menyimpan dengan 'jenis key-value'
		HashMap<Integer,String> map=new HashMap<Integer,String>();
		// map.put(K,V) adalah method untuk menambahkan key-value ke dalam object Map
		map.put(1,"Mango"); // menambahkan dengan kunci bernilai 1 dengan value "Mango" ke 'map'.
		map.put(2,"Apple"); // menambahkan dengan kunci bernilai 2 dengan value "Apple" ke 'map'.
		map.put(3,"Banana"); // menambahkan dengan kunci bernilai 3 dengan value "Mango" ke 'map'.
		map.put(4,"Grapes"); // menambahkan dengan kunci bernilai 4 dengan value "Mango" ke 'map'.
		map.put("pisang","Pisang"); // Terdapat error, karena menambahkan kunci dengan tipe String, harusnya Integer.

		System.out.println("Iterating Hashmap...");
		for(Map.Entry m : map.entrySet()){
			System.out.println(m.getKey()+" "+m.getValue());
		}
	}
}

/*
	Yang terjadi pada kode diatas ketika dijalankan ialah Error.
	Hal ini dikarenakan pada saat deklarasi HashMap, tipe Key adalah Integer dan tipe value String,
	pada saat map.put("pisang", "Pisang"), dimana key ialah String dengan nilai "pisang", sehingga
	menimbulkan error dimana object String tidak dapat diconvert ke Integer.
 */

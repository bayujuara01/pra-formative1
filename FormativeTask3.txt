// Jawaban Formative Task 3
// Untuk Kode Java terdapat pada folder 'code-pra1', FormativeTask31.java dan FormativeTask32.java

// Kode 1

import java.util.*; // melakukan import semua class pada library java.util yang merupakan java collection framework
/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class Task31.
*/
public class Task31 {
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan dan menerima argument tipe String.
	*/
	public static void main(String args[]) {
		// Deklarasi dan instansiasi variable 'list' dengan, tipe non-primitive ArrayList,
		// yang mana isi dalam ArrayList tersebut harus tipe object 'String'. 
		// ArrayList merupakan object seperti array tapi bersifat dinamis
		ArrayList<String> list = new ArrayList<String>();
		list.add("Mango"); // menambahkan object tipe 'String' dengan value 'Mango' ke list.
		list.add("Apple"); // menambahkan object tipe 'String' dengan value 'Apple' ke list.
		list.add("Banana"); // menambahkan object tipe 'String' dengan value 'Banana' ke list.
		list.add("Grapes"); // menambahkan object tipe 'String' dengan value 'Grapes' ke list.

		/*	Deklarasi dan inisialisasi variable 'itr' dengan, tipe non-primitive Iterator,
			yang mana nilai dari variable tersebut didapatkan dari eksekusi fungsi iterator yang terdapat 
			pada object ArrayList sehingga dengan object tipe Iterator, kita dapat melakukan iterate,
			dan mengakses tiap elemen pada arraylist.
		*/
		Iterator itr = list.iterator();
		/*	Melakukan perulangan pada iterator dengan mengecek dari awal iterator 'itr' jika memiliki nilai lanjut
			maka perulangan akan dilanjutkan jika tidak, perulangan terhenti.
		*/
		while (itr.hasNext()) { 
			System.out.println(itr.next()); // Menampilkan atau mencitak nilai dari iterator.next() pada tiap perulangan.
		}
		/*
			Sehingga Menampikan :
				Mango
				Apple
				Banana
				Grapes
		*/
	}
}

# Kode 2

import java.util.*; // melakukan import semua class pada library java.util yang merupakan java collection framework
/* 
	Kelas dengan modifier public dimana memiliki arti kelas ini dapat
	diakses oleh kelas lain baik didalam package atau diluar package yang sama
	dan nama class Task32.
*/
public class Task32 {
	/* 
		Method main yang merupakan entrypoint dan fungsi yang pertama kali
		dieksekusi ketika program dijalankan dan menerima argument tipe String.
	*/
	public static void main(String args[]) {
		// Deklarasi dan instansiasi variable 'list' dengan, tipe non-primitive ArrayList,
		// yang mana isi dalam ArrayList tersebut harus tipe object 'String'. 
		// ArrayList merupakan object seperti array tapi bersifat dinamis
		ArrayList<String> list = new ArrayList<String>();
		list.add("Mango"); // menambahkan object tipe 'String' dengan value 'Mango' ke list.
		list.add("Apple"); // menambahkan object tipe 'String' dengan value 'Apple' ke list.
		list.add("Banana"); // menambahkan object tipe 'String' dengan value 'Banana' ke list.
		list.add("Grapes"); // menambahkan object tipe 'String' dengan value 'Grapes' ke list.
		
		/*	Menampilkan dan mencetak representasi nilai-nilai atau values yang terdapat pada ArrayList 'list'
		 	Karena secara implisit akan memanggil method .toString() yang dimiliki oleh object ArrayList
		 	dimana mengubah nilai-nilai didalam ArrayList menjadi String.
		 	Sehingga akan menampilkan : [Mango, Apple, Banana, Grapes]
		*/
		System.out.println(list); 
	}
}

/* 
	Perbedaan : Pada class Task31, nilai ArrayList 'list' akan dilakukan iterasi satu persatu nilainya
		dalam perulangan kemudian ditampilkan dengan 'println' sehingga akan menambahkan breakline.
		Sedangkan pada class Task31, secara langsung menampilkan representasi nilai-nilai ArrayList,
		Karena secara implisit ketika menggunakan 'println' ArrayList 'list' akan otomatis memanggil
		method '.toString()' yang berguna untuk menjadikan String representasi dari nilai 'list'.
*/


